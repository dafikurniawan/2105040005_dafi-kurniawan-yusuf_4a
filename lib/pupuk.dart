import 'package:flutter/material.dart';

class Pupuk extends StatelessWidget {
  const Pupuk({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Pupuk"),
          backgroundColor:
              Color(0xFF0EDC85), // Ubah warna AppBar menjadi 0EDC85
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(
                  context); // Menutup halaman saat tombol panah kembali ditekan
            },
          ),
        ),
        body: const Placeholder(),
      ),
    );
  }
}
