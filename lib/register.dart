import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:login_app_flutter/home.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({super.key});

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController passwordValController = TextEditingController();

  bool errorValidator = false;

  Future signUp() async {
    try {
      await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: emailController.text,
        password: passwordController.text,
      );
      await FirebaseAuth.instance.signOut();
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        print('The account already exists for that email.');
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff0add9d),
      body: Padding(
        padding: const EdgeInsets.all(30.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.asset(
              'assets/petanidigital.png', // Ubah dengan path gambar Anda
              width: 450, // Sesuaikan ukuran gambar sesuai kebutuhan
              height: 250,
            ),
            SizedBox(width: 10), // Berikan jarak antara gambar dan teks
            Text(
              "Registration Now!",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 43.2,
                fontWeight: FontWeight.w600,
                fontFamily: "Poppins",
                color: Colors.white,
              ),
            ),
            SizedBox(width: 15),
            Text(
              "Email Address",
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            TextField(
              controller: emailController,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide.none),
                hintText: "Tuliskan Email",
                hintStyle: TextStyle(
                  color: Color(0xff0add9d),
                ),
                fillColor: Color(0xffd9d9d9),
                filled: true,
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Text(
              "Password",
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            TextField(
              controller: passwordController,
              obscureText: true,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide.none),
                hintText: "Tuliskan Password",
                hintStyle: TextStyle(
                  color: Color(0xff0add9d),
                ),
                fillColor: Color(0xffd9d9d9),
                filled: true,
                errorText: errorValidator ? "Password Tidak Sama!!" : null,
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Text(
              "Validasi Password",
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            TextField(
              controller: passwordValController,
              obscureText: true,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: BorderSide.none),
                hintText: "Ulangi Password",
                hintStyle: TextStyle(
                  color: Color(0xff0add9d),
                ),
                fillColor: Color(0xffd9d9d9),
                filled: true,
                errorText: errorValidator ? "Password Tidak Sama!!" : null,
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            SizedBox(
              width: double.infinity,
              height: 50.0,
              child: ElevatedButton(
                style: ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(
                        Color.fromARGB(255, 255, 215, 64))),
                onPressed: () {
                  setState(() {
                    passwordController.text == passwordValController.text
                        ? errorValidator = false
                        : errorValidator = true;
                  });
                  if (errorValidator) {
                    print("Error");
                  } else {
                    signUp();
                    Navigator.pop(context);
                  }
                },
                child: Text("Create"),
              ),
            ),
            SizedBox(
              height: 50.0,
            ),
            Center(
              child: TextButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => RegisterPage(),
                    ),
                  );
                },
                child: Container(
                  child: Column(
                    children: [
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(
                        "Already have an Account? Log In",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 10.0,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
