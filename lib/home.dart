import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:login_app_flutter/informasi.dart';
import 'package:login_app_flutter/konsultasi.dart';
import 'package:login_app_flutter/pupuk.dart';
import 'package:login_app_flutter/tips.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key});

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late PageController _pageController;
  List<String> images = [
    'assets/gambar1.png',
    'assets/gambar2.png',
    'assets/gambar3.png',
  ];
  int _selectedIndex = 0;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(viewportFraction: 0.8);
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: Row(
          children: [
            Image.asset(
              'assets/petanidigital.png',
              width: 80,
              height: 80,
            ),
            SizedBox(width: 6),
            Text(
              "MyFarm",
              style: TextStyle(
                fontSize: 26,
                fontWeight: FontWeight.bold,
                color: Color(0xFF0EDC85),
              ),
            ),
          ],
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.menu),
            color: Colors.black,
            onPressed: () {},
          ),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              child: PageView.builder(
                itemCount: images.length,
                controller: _pageController,
                itemBuilder: (context, index) {
                  return Container(
                    margin: EdgeInsets.all(10),
                    child: Image.asset(
                      images[index],
                    ),
                  );
                },
              ),
            ),
            SizedBox(height: 40),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Container(
                width: 350,
                height: 420,
                decoration: BoxDecoration(
                  color: Color(0xFFDDDDDD),
                  borderRadius: BorderRadius.circular(18),
                ),
                child: Column(
                  children: [
                    SizedBox(height: 0),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Transform(
                        transform: Matrix4.translationValues(0, -18, 0),
                        child: Container(
                          width: 135,
                          height: 42.46,
                          decoration: BoxDecoration(
                            color: Color(0xFF0FA958),
                            borderRadius: BorderRadius.circular(12),
                          ),
                          child: Center(
                            child: Text(
                              'LAYANAN',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Poppins',
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Row(
                        children: [
                          Expanded(
                            child: Align(
                              alignment: Alignment.topCenter,
                              child: Transform(
                                transform: Matrix4.translationValues(0, -16, 0),
                                child: InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => Informasi(),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    margin: EdgeInsets.all(20),
                                    width: 120,
                                    height: 400,
                                    decoration: BoxDecoration(
                                      color: Color(0xFFD4F2E0),
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                                    child: Stack(
                                      children: [
                                        Positioned(
                                          top: 26,
                                          right: 35,
                                          child: Container(
                                            width: 55,
                                            height: 59,
                                            decoration: BoxDecoration(
                                              color: Colors.white,
                                              shape: BoxShape.circle,
                                            ),
                                            child: ClipOval(
                                              child: Image.asset(
                                                'assets/gambar4.png', // Ganti dengan path gambar yang Anda inginkan
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ),
                                        ),
                                        Positioned(
                                          bottom: 10,
                                          left: 0,
                                          right: 0,
                                          child: Text(
                                            'Informasi Produksi',
                                            textAlign: TextAlign.center,
                                            style: GoogleFonts.poppins(
                                              fontSize: 16,
                                              fontWeight: FontWeight.w600,
                                              color: Color(0xFF0FA958),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            child: Align(
                              alignment: Alignment.topCenter,
                              child: Transform(
                                transform: Matrix4.translationValues(0, -16, 0),
                                child: InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => Pupuk(),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    margin: EdgeInsets.all(20),
                                    width: 120,
                                    height: 400,
                                    decoration: BoxDecoration(
                                      color: Color(0xFFD4F2E0),
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                                    child: Stack(
                                      children: [
                                        Positioned(
                                          top: 26,
                                          right: 32,
                                          child: Container(
                                            width: 55,
                                            height: 59,
                                            decoration: BoxDecoration(
                                              color: Colors.white,
                                              shape: BoxShape.circle,
                                            ),
                                            child: ClipOval(
                                              child: Image.asset(
                                                'assets/gambar5.png', // Ganti dengan path gambar yang Anda inginkan
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ),
                                        ),
                                        Positioned(
                                          bottom: 25,
                                          left: 0,
                                          right: 0,
                                          child: Text(
                                            'Pupuk',
                                            textAlign: TextAlign.center,
                                            style: GoogleFonts.poppins(
                                              fontSize: 16,
                                              fontWeight: FontWeight.w600,
                                              color: Color(0xFF0FA958),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Row(
                        children: [
                          Expanded(
                            child: Align(
                              alignment: Alignment.topCenter,
                              child: Transform(
                                transform: Matrix4.translationValues(0, -25, 0),
                                child: InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => Tips(),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    margin: EdgeInsets.all(20),
                                    width: 120,
                                    height: 400,
                                    decoration: BoxDecoration(
                                      color: Color(0xFFD4F2E0),
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                                    child: Stack(
                                      children: [
                                        Positioned(
                                          top: 26,
                                          right: 32,
                                          child: Container(
                                            width: 55,
                                            height: 59,
                                            decoration: BoxDecoration(
                                              color: Colors.white,
                                              shape: BoxShape.circle,
                                            ),
                                            child: ClipOval(
                                              child: Image.asset(
                                                'assets/gambar6.png', // Ganti dengan path gambar yang Anda inginkan
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ),
                                        ),
                                        Positioned(
                                          bottom: 25,
                                          left: 0,
                                          right: 0,
                                          child: Text(
                                            'Tips & Trik',
                                            textAlign: TextAlign.center,
                                            style: GoogleFonts.poppins(
                                              fontSize: 16,
                                              fontWeight: FontWeight.w600,
                                              color: Color(0xFF0FA958),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            child: Align(
                              alignment: Alignment.topCenter,
                              child: Transform(
                                transform: Matrix4.translationValues(0, -25, 0),
                                child: InkWell(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => Konsultasi(),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    margin: EdgeInsets.all(20),
                                    width: 120,
                                    height: 400,
                                    decoration: BoxDecoration(
                                      color: Color(0xFFD4F2E0),
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                                    child: Stack(
                                      children: [
                                        Positioned(
                                          top: 26,
                                          right: 32,
                                          child: Container(
                                            width: 55,
                                            height: 59,
                                            decoration: BoxDecoration(
                                              color: Colors.white,
                                              shape: BoxShape.circle,
                                            ),
                                            child: ClipOval(
                                              child: Image.asset(
                                                'assets/gambar7.png', // Ganti dengan path gambar yang Anda inginkan
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ),
                                        ),
                                        Positioned(
                                          bottom: 25,
                                          left: 0,
                                          right: 0,
                                          child: Text(
                                            'Konsultasi',
                                            textAlign: TextAlign.center,
                                            style: GoogleFonts.poppins(
                                              fontSize: 16,
                                              fontWeight: FontWeight.w600,
                                              color: Color(0xFF0FA958),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
        backgroundColor: Color(0xFF0EDC85),
        selectedIconTheme: IconThemeData(color: Color(0xFF008000)),
        unselectedIconTheme: IconThemeData(color: Colors.white),
        selectedItemColor: Color(0xFF008000),
        unselectedItemColor: Colors.white,
        selectedLabelStyle: TextStyle(color: Color(0xFF008000)),
        unselectedLabelStyle: TextStyle(color: Colors.white),
        selectedFontSize: 16.0,
        unselectedFontSize: 12.0,
        items: [
          BottomNavigationBarItem(
            icon: Transform.scale(
              scale: _selectedIndex == 0 ? 1.2 : 1.0,
              child: Icon(Icons.home),
            ),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Transform.scale(
              scale: _selectedIndex == 1 ? 1.2 : 1.0,
              child: Icon(Icons.search),
            ),
            label: 'Search',
          ),
          BottomNavigationBarItem(
            icon: Transform.scale(
              scale: _selectedIndex == 2 ? 1.2 : 1.0,
              child: Icon(Icons.notification_add),
            ),
            label: 'Notification',
          ),
          BottomNavigationBarItem(
            icon: Transform.scale(
              scale: _selectedIndex == 3 ? 1.2 : 1.0,
              child: Icon(Icons.account_circle),
            ),
            label: 'Profile',
          ),
        ],
      ),
    );
  }
}
